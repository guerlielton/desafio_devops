#!/bin/bash

# get version from VERSION file
VERSION=$(cat VERSION)

# build container from Dockerfile using Gitlab tag
docker build -t ps-hello-server .

# tag for deploy in gitlab
# docker tag registry.gitlab.com/aristofanio/hello-server:$VERSION
# docker push registry.gitlab.com/aristofanio/hello-server:$VERSION
# docker push registry.gitlab.com/aristofanio/hello-server:latest