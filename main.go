package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
)

func main() {
	//
	log.Printf("Starting server")
	// handle request
	http.HandleFunc("/_status/healthz", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ok")
	})
	http.HandleFunc("/helo", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello2, %q", html.EscapeString(r.URL.Path))
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Please try /helo")
	})
	// start and listen on server
	http.ListenAndServe(":8080", nil)
}
