# Desafio Devops

Obrigado por participar do nosso processo seletivo. 

Entendemos que é difícil encontrar tempo principalmente quando está trabalhando, por isso agradecemos o seu tempo e dedicação.

O código produzido deve estar versionado em algum repositório público (Github, Bitbucket etc.).

Quando estiver tudo pronto, por favor, contate-nos pelo mesmo canal que recebeu o desafio com link para o seu repositório contendo todos os arquivos de configurações, scripts e outras informações que achar relevante apresentar. 

## 1) Setup para K8s.

A primeira tarefa é preparar duas máquinas para trabalhar como manager e worker.
Utilize kubeadm (https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/) para criar um cluster e configura-lo para receber as aplicações. 
Utilize a configuração inicial abaixo para fazer um deploy de uma aplicação de demonstração. Posteriormente mapeie esta aplicação para a porta 30001 e a deixe acessível no worker.

```
#Deployment
#nginx-dploy.yml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deploy
  labels:
    app: nginx-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-app
  template:
    metadata:
      labels:
        app: nginx-app
    spec:
      containers:
      - name: nginx-container
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

## 2) Liberando applicação para acesso externo

Faça o deploy da aplicação contida neste repositório e mapei-a dentro do NGINX para que seja possível acessá-la pelo caminho /happ, por exemplo:
```
curl -X GET http://<IP>:30001/happ/_status/healthz
# esperado resultado: ok
```
## Resultados

Realizei as configurações com duas formas uma usando o ingress-nginx com k8s e outra realizando a instalação do nginx localmente para utilizar o proxy pass deixei a aplicação exposta no ip da MAQ3 com dois caminhos e portas distintas
```
curl ip:30001/happ/_status/healthz #para o ingress-nginx com container e curl ip:30002/happ/_status/healthz #para o proxy pass com nginx
```
Após isso utilizei o comando ```kubectl apply -f ``` para aplicar as configurações contidas nos arquivos.yaml a configuração que está no arquivo```ingress-v1.yaml``` tem o ```nginx.ingress.kubernetes.io/rewrite-target:``` que ira expor caminho /happ para nossa aplicação
 
Já com o proxy_pass no container do nginx realizei a configuração apontando service gerado através do deploy do arquivo ``` service.yaml```com o location no caminho /happ/ e proxy_pass http://hserver-service:8080/;
 
Abaixo fiz a configuração para resolvermos os ips por nome fiz isso dentro do ```/etc/hosts```
```
root@Manager# nslookup hserver-proxy.com
Server:   127.0.0.53
Address:  127.0.0.53#53
Non-authoritative answer:
Name: hserver-proxy.com
Address: 10.109.184.222
 
root@Manager# nslookup hserver-ingress.com
Server:   127.0.0.53
Address:  127.0.0.53#53
Non-authoritative answer:
Name: hserver-ingress.com
Address: 10.102.64.98
```
Para instalar o ingress-nginx ultizei o comando  ``` kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud/deploy.yaml```  ele vai aplicar tudo que e necessario para o ingress-nginx funcionar o tipo dele e loadbalance, apos isso criei os arquivos de ingress com o  ```rewrite-target```  este arquivo ficou contido dentro da pasta k8s.Apos todos os arquivos deployados obtemos o resultado abaixo.
```
root@guerlielton/Arquivos/DevOps# curl hserver-proxy.com:30002/happ/_status/healthz
ok
root@guerlielton/Arquivos/DevOps#
#Para acessar com o name alterei o arquivo /etc/hosts da minha máquina local passando o IP-Público da MAQ3 e o os nomes.
root@guerlielton/Arquivos/DevOps# curl hserver-ingress.com:30001/happ/_status/healthz
ok
root@guerlielton/Arquivos/DevOps#
```
Dentro do arquivo do nginx local em ```/etc/nginx/sites-available/``` configurei o arquivo ```default``` da seguinte maneira
```
server {
  listen 30002;
  server_name localhost;
  location / {
      root /usr/share/nginx/html;
      index index.html;
   }
  location /happ/ {
      proxy_pass http://hserver-proxy.com:8080/;
   }
}
```
Desta maneira ele irá bater no ip do service/ingress-nginx-controller e fará o redirecionamento para a aplicação
 
 
As imagens geradas no desafio ficaram em meu registry do gitlab e demais arquivos estão criado dentro da Ec2 MQ3
 
 



